import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Containers from './components/Containers/Containers';
import Inputs from './components/Inputs/Inputs';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/containers' element={<Containers />} />
        <Route path='/inputs' element={<Inputs />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
