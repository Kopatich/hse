import { PhotoCamera } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import {
    Button,
    IconButton,
    Stack,
    Container,
    Checkbox,
    FormControlLabel,
    FormGroup,
    FormControl,
    FormLabel,
    Radio,
    RadioGroup,
    TextField
} from '@mui/material';
import React from 'react';

const Inputs = () => {
    return (
        <Container>
            <Stack spacing={2}>
                <Stack spacing={2} direction="row">
                    <Button variant="text">Text</Button>
                    <Button variant="contained">Contained</Button>
                    <Button variant="outlined">Outlined</Button>
                    <Button variant="contained" disabled> Disabled</Button>
                </Stack>
                <Stack spacing={2} direction='row'>
                    <Button variant="contained" component="label">
                        Upload
                        <input hidden accept="image/*" multiple type="file" />
                    </Button>
                    <IconButton color="primary" aria-label="upload picture" component="label">
                        <input hidden accept="image/*" type="file" />
                        <PhotoCamera />
                    </IconButton>
                </Stack>
                <Stack spacing={2} direction='row'>
                    <Button variant="outlined" startIcon={<DeleteIcon />} size='small' color='error'>
                        Delete
                    </Button>
                    <Button variant="contained" endIcon={<SendIcon />} size='large' color='success'>
                        Send
                    </Button>
                </Stack>
            </Stack>

            <FormGroup>
                <FormControlLabel control={<Checkbox defaultChecked />} label="Checkbox label" />
                <FormControlLabel disabled control={<Checkbox />} label="Disabled" />
                <FormControlLabel control={<Checkbox icon={<DeleteIcon />} checkedIcon={<SendIcon />} />} label="Iconized" />
            </FormGroup>

            <FormControl>
                <FormLabel id="demo-row-radio-buttons-group-label">Gender</FormLabel>
                <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                >
                    <FormControlLabel value="female" control={<Radio />} label="Female" />
                    <FormControlLabel value="male" control={<Radio />} label="Male" />
                    <FormControlLabel value="other" control={<Radio />} label="Other" />
                    <FormControlLabel
                        value="disabled"
                        disabled
                        control={<Radio />}
                        label="other"
                    />
                </RadioGroup>
            </FormControl>

            <Stack spacing={2} direction='row'>
                <TextField id="outlined-basic" label="Outlined" variant="outlined" defaultValue='Pass text here' InputLabelProps={{ shrink: true }}/>
                <TextField id="filled-basic" label="Filled" variant="filled" multiline/>
                <TextField id="standard-basic" label='You can highlight as error' variant="standard" error placeholder='You can highlight as error'/>
            </Stack>
        </Container>
    );
}

export default Inputs;