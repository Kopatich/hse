import { Container, Box, Stack, Divider, Grid } from '@mui/material';
import React from 'react';
import logo from '../../logo.svg';

const StyledSpan = (props: { children?: React.ReactNode }) => (
    <span style={{fontSize: '24pt',}}>{props.children}</span>
)

const Containers = () => {
    return (
        <Container>
            <Box maxWidth={500}>
                <img src={logo} alt="" />
            </Box>
            <Stack divider={<Divider />}>
                <Box><StyledSpan>Text 1</StyledSpan></Box>
                <Box><StyledSpan>Text 2</StyledSpan></Box>
                <Box><StyledSpan>Text 3</StyledSpan></Box>
            </Stack>
            <Grid container sx={{border: '1px solid red'}}>
                <Grid item xs={6} sx={{border: '1px solid black'}}>
                    <Box>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores qui explicabo possimus dicta debitis cupiditate laboriosam sint dolorum expedita id fugit voluptatibus, quo magni sed doloribus quibusdam neque aliquam numquam.</Box>
                </Grid>
                <Grid xs={5} sx={{border: '1px solid black'}}>
                    <Box>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores qui explicabo possimus dicta debitis cupiditate laboriosam sint dolorum expedita id fugit voluptatibus, quo magni sed doloribus quibusdam neque aliquam numquam.</Box>
                </Grid>
                <Grid xs={3} sx={{border: '1px solid black'}}>
                    <Box>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores qui explicabo possimus dicta debitis cupiditate laboriosam sint dolorum expedita id fugit voluptatibus, quo magni sed doloribus quibusdam neque aliquam numquam.</Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default Containers;